package com.my.micro.service.user.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Zijian Liao
 * @since 1.0.0
 */
@Slf4j
@Order
@ConditionalOnClass(HttpServletRequest.class)
@RestControllerAdvice
public class MyExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Map<String, String>> handlerException(Exception ex) {
        log.error("运行时异常：", ex);
        Map<String, String> map = new HashMap<>();
        map.put("code", "-1");
        map.put("message", "失败");
        return  ResponseEntity.ok(map);
    }

    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity<Map<String, String>> handlerException(NullPointerException ex) {
        log.error("运行时异常：", ex);
        Map<String, String> map = new HashMap<>();
        map.put("code", "-1");
        map.put("message", "失败");
        return  ResponseEntity.ok(map);
    }
}
