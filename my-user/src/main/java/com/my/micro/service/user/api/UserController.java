package com.my.micro.service.user.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Zijian Liao
 * @since 1.0.0
 */
@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

    @GetMapping("/get")
    public String get(){
        return "my name is 张四";
    }

    @GetMapping("/test")
    public String test(){
        try{
            String a = null;
            System.out.println(a.length());
        }catch (Exception e){
            log.error("xxx", e);
        }

        return "my name is 张四";
    }
}
