package com.my.micro.service.order.feign;

import com.my.micro.service.order.domain.Goods;
import com.my.micro.service.order.result.BaseResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author Zijian Liao
 * @since 1.0.0
 */
@FeignClient(name = "my-goods", path = "/goods", contextId = "goods", fallbackFactory = GoodsApiFallback.class)
public interface GoodsApi {

    @GetMapping("/get-goods")
    Goods getGoods();

    /**
     * get 方式传参加上需@SpringQueryMap注解
     */
    @GetMapping("/goods")
    BaseResult<Goods> getGoods(@SpringQueryMap Goods goods);

    @GetMapping("/list")
    List<Goods> list();

    @PostMapping(value = "/save")
    void save(Goods goods);

    @DeleteMapping
    void delete(String id);
}
