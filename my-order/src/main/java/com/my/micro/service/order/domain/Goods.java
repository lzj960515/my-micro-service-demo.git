package com.my.micro.service.order.domain;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author Zijian Liao
 * @since 1.0.0
 */
@Data
@Accessors(chain = true)
public class Goods {

    private String name;
    private double price;
    private int number;

}
