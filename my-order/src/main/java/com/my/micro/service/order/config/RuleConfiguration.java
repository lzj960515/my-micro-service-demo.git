package com.my.micro.service.order.config;

import com.alibaba.cloud.nacos.ribbon.NacosRule;
import com.my.micro.service.order.rule.VersionRule;
import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RetryRule;
import com.netflix.loadbalancer.RoundRobinRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 负载均衡规则配置
 *
 * @author Zijian Liao
 * @since 1.0.0
 */
@Configuration
public class RuleConfiguration {

//    @Bean
//    public IRule rule(){
//        return new VersionRule();
//    }
}
