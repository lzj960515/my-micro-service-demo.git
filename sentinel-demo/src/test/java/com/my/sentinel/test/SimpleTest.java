package com.my.sentinel.test;

import com.alibaba.csp.sentinel.Entry;
import com.alibaba.csp.sentinel.SphU;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.csp.sentinel.slots.block.RuleConstant;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRule;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRuleManager;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Zijian Liao
 * @since 1.0.0
 */
@Slf4j
public class SimpleTest {
    // 记录请求总数
    private static final AtomicInteger TOTAL = new AtomicInteger();
    // 记录请求通过数
    private static final AtomicInteger PASS = new AtomicInteger();
    // 记录请求拒绝数
    private static final AtomicInteger BLOCK = new AtomicInteger();


    @Test
    public void testBlock() throws IOException {
        // 初始化规则
        this.initFlowRules();
        // 模拟高并发访问
        this.request();
        // 统计qps
        this.count();
        System.in.read();
    }

    private void request() {
        for (int i = 0; i < 30; i++) {
            new Thread(() -> {
                while (true){
                    // 记录总qps
                    TOTAL.incrementAndGet();
                    // 进行限流
                    try (Entry entry = SphU.entry("HelloWorld")) {
                        // 记录通过数
                        PASS.incrementAndGet();
                    } catch (BlockException e) {
                        // 记录拒绝数
                        BLOCK.incrementAndGet();
                    }
                    // 模拟业务等待0-50毫秒
                    try {
                        TimeUnit.MILLISECONDS.sleep(new Random().nextInt(50));
                    } catch (InterruptedException ignored) {
                    }
                }
            }).start();
        }
    }

    public void count() {
        new Thread(() -> {
            int oldTotal = 0, oldPass = 0, oldBlock = 0;
            while (true){
                // 计算当前qps
                int total = TOTAL.get();
                int secondsTotal = total - oldTotal;
                oldTotal = total;

                // 计算每秒通过数
                int pass = PASS.get();
                int secondsPass = pass - oldPass;
                oldPass = pass;

                // 计算每秒拒绝数
                int block = BLOCK.get();
                int secondsBlock = block - oldBlock;
                oldBlock = block;

                log.info("当前qps：{}, pass: {}, block:{}", secondsTotal, secondsPass, secondsBlock);
                try {
                    // 停顿一秒
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException ignored) {
                }
            }
        }).start();
    }

    @Test
    public void ip() throws UnknownHostException {
        InetAddress address = InetAddress.getLocalHost();
        log.info(address.getHostAddress());
    }

    private void initFlowRules() {
        {
            // 定义流控规则
            FlowRule rule = new FlowRule();
            // 资源名与需要限流的资源名相同
            rule.setResource("HelloWorld");
            // 设置限流方式为QPS
            rule.setGrade(RuleConstant.FLOW_GRADE_QPS);
            // 设置QPS为20
            rule.setCount(20);
            // 加载规则
            FlowRuleManager.loadRules(Collections.singletonList(rule));
        }
        // 此处测试同一规则重复加载效果，测试结果：覆盖原有规则
        {
            // 定义流控规则
            FlowRule rule = new FlowRule();
            // 资源名与需要限流的资源名相同
            rule.setResource("HelloWorld");
            // 设置限流方式为QPS
            rule.setGrade(RuleConstant.FLOW_GRADE_QPS);
            // 设置QPS为20
            rule.setCount(15);
            // 加载规则
            FlowRuleManager.loadRules(Collections.singletonList(rule));
        }
    }

}
