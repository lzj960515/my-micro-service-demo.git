package com.my.sentinel.service;

/**
 * @author Zijian Liao
 * @since 1.0.0
 */
public interface FooService {

    String hello(String name);
}
