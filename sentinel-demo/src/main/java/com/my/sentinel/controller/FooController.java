package com.my.sentinel.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.my.sentinel.service.FooService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

/**
 * @author Zijian Liao
 * @since 1.0.0
 */
@RestController
@RequestMapping("/foo")
public class FooController {

    @Autowired
    private FooService fooService;

    @GetMapping
    public String hello(String name) {
        return fooService.hello(name);
    }

    @GetMapping("/test")
    public String test() {
        return fooService.hello("test");
    }

    @GetMapping("/test2")
    public String test2() {
        return fooService.hello("test2");
    }

    @GetMapping("/test3")
    public String test3() throws InterruptedException {
        TimeUnit.SECONDS.sleep(1);
        return "ok";
    }

    @GetMapping("/a")
    public String a(String code, String state){
        System.out.println("code:" + code + " state:" + state);
        return "ok";
    }


}
