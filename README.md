# 微服务示例
- my-goods 商品服务 服务消费者
- my-order 订单服务 服务提供者
- my-sidecar 边车 用于外挂积分服务
- my-integral 积分服务 服务提供者 本身不集成注册中心，由边车与注册中心通信