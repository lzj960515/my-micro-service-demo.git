package com.my.micro.service.inquiry.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Zijian Liao
 * @since 1.0.0
 */
@RestController
@RequestMapping("/inquiry")
public class InquiryController {

    @GetMapping("/hello")
    public String hello(){
        return "我张四来看病了！";
    }
}
