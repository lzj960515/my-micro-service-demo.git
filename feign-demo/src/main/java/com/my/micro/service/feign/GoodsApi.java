package com.my.micro.service.feign;

import feign.Headers;
import feign.Param;
import feign.RequestLine;

import java.util.List;

/**
 * 商品服务原生feign接口
 * @author Zijian Liao
 * @since 1.0.0
 */
public interface GoodsApi {
    /**
     * 获取商品信息
     */
    @RequestLine("GET /goods/get-goods")
    Goods getGoods();

    @RequestLine("GET /goods/list")
    List<Goods> list();

    @RequestLine("POST /goods/save")
    @Headers("Content-Type: application/json")
    void save(Goods goods);

    @RequestLine("DELETE /goods?id={id}")
    @Headers("Content-Type: application/json")
    void delete(@Param("id") String id);
}
