package com.my.micro.service.integral;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 积分服务
 *
 * @author Zijian Liao
 * @since 1.0.0
 */
@SpringBootApplication
public class IntegralApplication {

    public static void main(String[] args) {
        SpringApplication.run(IntegralApplication.class, args);
    }


}
