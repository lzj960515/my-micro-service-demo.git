package com.my.micro.service.good.result;

import lombok.Data;

/**
 * @author Zijian Liao
 * @since 1.0.0
 */
@Data
public class BaseResult<T> {

    private Integer code;

    private String message;

    private T data;

    public BaseResult(Integer code, String message){
        this.code = code;
        this.message = message;
    }

    public BaseResult(Integer code, String message, T data){
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static <T> BaseResult<T> success(T data){
        return new BaseResult<>(0, "成功", data);
    }

    public static BaseResult<Void> fail(String message){
        return new BaseResult<>(-1, message);
    }
}
