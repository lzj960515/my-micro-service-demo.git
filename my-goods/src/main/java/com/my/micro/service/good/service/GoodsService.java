package com.my.micro.service.good.service;

import com.alibaba.cloud.nacos.NacosDiscoveryProperties;
import com.alibaba.cloud.nacos.NacosServiceManager;
import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.api.naming.pojo.Instance;
import com.my.micro.service.good.domain.Order;
import com.my.micro.service.good.feign.OrderServer;
import com.my.micro.service.good.properties.GoodsProperties;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * @author Zijian Liao
 * @since 1.0.0
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class GoodsService {

    private final DiscoveryClient discoveryClient;
    private final RestTemplate restTemplate;
    private final OrderServer orderServer;

    public String order() {
//        String url = "http://" + getDomainByWeight("my-order") + "/order/order";
//        String url = "http://my-order/order/order";
//        return restTemplate.getForObject(url, String.class);
        return orderServer.order();
    }

    public Order ex() {
        Order ex = orderServer.ex();
        System.out.println(ex);
        return ex;
    }

    public Order get() {
        Order ex = orderServer.ex();
        System.out.println(ex);
        return ex;
    }

    private String getDomain(String serviceId){
        List<ServiceInstance> producer = discoveryClient.getInstances(serviceId);
        ServiceInstance serviceInstance = producer.get(0);
        String host = serviceInstance.getHost();
        int port = serviceInstance.getPort();
        String url = host + ":" + port;
        log.info(url);
        return url;
    }

    private final NacosDiscoveryProperties discoveryProperties;

    private final NacosServiceManager nacosServiceManager;

    private NamingService namingService() {
        return nacosServiceManager
                .getNamingService(discoveryProperties.getNacosProperties());
    }

    private String getDomainByWeight(String serviceId){
        Instance instance = null;
        try {
            instance = namingService().selectOneHealthyInstance(serviceId);
        } catch (NacosException e) {
            e.printStackTrace();
        }
        String host = instance.getIp();
        int port = instance.getPort();
        String url = host + ":" + port;
        log.info(url);
        return url;
    }


    //-------------配置功能------------

    private final GoodsProperties goodsProperties;

    public String config(){
        log.info("name: {}, price: {}, number: {} ", goodsProperties.getName(),
                goodsProperties.getPrice(), goodsProperties.getNumber());
        String result = "name: %s, price: %s, number: %s";
        return String.format(result, goodsProperties.getName(),
                goodsProperties.getPrice(), goodsProperties.getNumber());
    }

    //-------------边车模式------------

    public String integral(){
        String url = "http://my-integral/integral/remain";
        return restTemplate.getForObject(url, String.class);
    }

}
