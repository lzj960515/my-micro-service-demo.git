package com.my.micro.service.good.feign.fallback;

import com.my.micro.service.good.domain.Order;
import com.my.micro.service.good.feign.OrderServer;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author Zijian Liao
 * @since 1.0.0
 */
@Slf4j
@Component
public class OrderServerFallback implements FallbackFactory<OrderServer> {

    @Override
    public OrderServer create(Throwable throwable) {
        log.error(throwable.getMessage(), throwable);
        return new OrderServer() {
            @Override
            public String order() {
                return "xxxxx";
            }

            @Override
            public Order ex() {
                log.error("error");
                return null;
            }

            @Override
            public Order get() {
                log.error("error");
                return null;
            }
        };
    }
}
