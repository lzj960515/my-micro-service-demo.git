package com.my.micro.service.good.feign;

import com.my.micro.service.good.domain.Order;
import com.my.micro.service.good.feign.fallback.OrderServerFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author Zijian Liao
 * @since 1.0.0
 */
@FeignClient(value = "my-order22", url = "${order.url}", path = "/order", fallbackFactory = OrderServerFallback.class)
public interface OrderServer {

    @GetMapping("/order")
    String order();

    @GetMapping("/ex")
    Order ex();

    @GetMapping("/get")
    Order get();
}
