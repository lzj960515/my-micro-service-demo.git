package com.my.micro.service.good.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;

/**
 * @author Zijian Liao
 * @since 1.0.0
 */
@Data
@RefreshScope
@ConfigurationProperties(prefix = "fruit")
public class GoodsProperties {

    private String name;
    private double price;
    private int number;

}
