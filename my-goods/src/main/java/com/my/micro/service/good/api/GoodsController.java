package com.my.micro.service.good.api;

import com.my.micro.service.good.domain.Goods;
import com.my.micro.service.good.domain.Order;
import com.my.micro.service.good.properties.GoodsProperties;
import com.my.micro.service.good.result.BaseResult;
import com.my.micro.service.good.service.GoodsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author Zijian Liao
 * @since 1.0.0
 */
@Slf4j
@RefreshScope
@RestController
@RequestMapping("/goods")
@RequiredArgsConstructor
public class GoodsController {

    @Autowired
    private GoodsProperties goodsProperties;

    @GetMapping("/all")
    public String all(){
        String result = "name: %s, price: %s, number: %s";
        return String.format(result, goodsProperties.getName(),
                goodsProperties.getPrice(), goodsProperties.getNumber());
    }

    @GetMapping("/get-goods")
    public BaseResult<Goods> getGoods() throws InterruptedException {
        System.out.println("xxxxxxx");
        return BaseResult.success(new Goods().setName("苹果")
                .setPrice(1.1)
                .setNumber(2));
    }

    /*@GetMapping("/get-goods")
    public Goods getGoods() throws InterruptedException {
        TimeUnit.SECONDS.sleep(10);
        System.out.println("xxxxxxx");
        return new Goods().setName("苹果")
                .setPrice(1.1)
                .setNumber(2);
    }*/

    @GetMapping("/list")
    public BaseResult<List<Goods>> list(){
        ArrayList<Goods> goodsList = new ArrayList<>();
        Goods apple = new Goods().setName("苹果")
                .setPrice(1.1)
                .setNumber(2);
        goodsList.add(apple);
        Goods lemon = new Goods().setName("柠檬")
                .setPrice(5.1)
                .setNumber(3);
        goodsList.add(lemon);
        return BaseResult.success(goodsList);
    }

    @PostMapping("save")
    public void save(@RequestBody Goods goods){
        System.out.println(goods);
    }

    @DeleteMapping
    public void delete(String id){
        System.out.println(id);
    }

    /**
     * 测试配置优先级
     */
    @Value("${filename}")
    public String filename;

    @GetMapping("/filename")
    public String filename(){
        return "now filename is " + filename;
    }

    @Value("${goods}")
    public String goods;

    @Value("${price}")
    public String price;

    @GetMapping("/config")
    public String config(){
        return "this goods is " + goods + ", and price is " + price;
    }

    @Value("${redis}")
    public String redis;

    @GetMapping("/redis")
    public String redis(){
        return "redis url is " + redis;
    }

    @Value("${server.port}")
    private Integer port;

    @GetMapping("/get")
    public String get() throws InterruptedException {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println( "商品服务响应了一个苹果" + port);
        return "商品服务响应了一个苹果" + port;
    }

    @PostMapping("/post")
    public String post(){
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println( "商品服务响应了一个苹果 post" + port);
        return "商品服务响应了一个苹果 post" + port;
    }


    private final GoodsService goodsService;

    @GetMapping("/order")
    public String order(){
        return goodsService.order();
    }

    @GetMapping("/ex")
    public Order ex(){
        return goodsService.ex();
    }

//    @GetMapping("/get")
//    public Order get(){
//        return goodsService.get();
//    }

    //-------------配置功能------------


     /*@GetMapping("/config")
    public String config(){
        return goodsService.config();
    }*/

    //-------------边车模式------------

    @GetMapping("/integral")
    public String integral(){
        return goodsService.integral();
    }
}
