package com.my.micro.service.gateway;

import org.junit.Test;

import java.time.ZoneId;
import java.time.ZonedDateTime;

/**
 * @author Zijian Liao
 * @since 1.0.0
 */
public class ZonedDateTimeTest {

    @Test
    public void generateDateTime(){
        ZonedDateTime zonedDateTime = ZonedDateTime.now(ZoneId.of("Asia/Shanghai"));
        System.out.println(zonedDateTime);
    }
}
